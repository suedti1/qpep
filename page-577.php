               <?php
get_header(); ?>

<script>
	    jQuery( document ).ready( function( $ ) {

<?php   


$comune_title= 	icl_t('parma', 'Filtra i contenuti per comune', 'Filtra i contenuti per comune');
$cat_title= 	icl_t('parma', 'Filtra i contenuti per categoria', 'Filtra i contenuti per categoria');


echo 'jQuery(".gmw-search-form-filters").prepend("<h4 class=\'cat-filter\'>'.$cat_title.'</h4>");';

echo 'jQuery(".city-pre").append("<h4 class=\'town-filter\'>'.$comune_title.'</h4>");';
 ?>
	  });

	  


</script>
               <div class="row full-w">
                    <div class="large-12 columns full-w-c">
                        <div class="wrapper">
	                        <!--
                            <a data-fancybox data-type="iframe" data-src="/proposta/" href="javascript:;"> -->
                            <?php 
	                            
	                            if(strpos( $_SERVER['HTTP_HOST'], 'quiprimaepoi.it') !== false){
								$translated_page = 2;
								}
								else{
									$translated_page = icl_object_id(1041, 'page', true);
									
								}
                            ?>
                            <a href="<?php echo get_permalink( $translated_page );?>" >
<div class=" float-center above-video btn-partecipa tr cat"><span class="partecipa-home bttn cat"><?php _e( 'PARTECIPA ANCHE TU.', 'Parma' ); ?> </span></div>                            </a>
                           <!-- <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logline-2.svg" class="slogan-home" id="p7"/> -->
                          
                            <section id="big-video"><a id="video"></a>
                            
                            <?php
	add_filter('acf/settings/current_language',function() {
			global $sitepress;
			return $sitepress->get_default_language();
		});
	if(ICL_LANGUAGE_CODE == 'de'){$img=get_field( 'banner_image_de', 'option');}
	if(ICL_LANGUAGE_CODE == 'hr'){$img=get_field( 'banner_image_hr', 'option');}
	if(ICL_LANGUAGE_CODE == 'fr'){$img=get_field( 'banner_image_fr', 'option');}
	if(ICL_LANGUAGE_CODE == 'es'){$img=get_field( 'banner_image_es', 'option');}
	if(ICL_LANGUAGE_CODE == 'sv'){$img=get_field( 'banner_image_sv', 'option');}
	add_filter('acf/settings/current_language',function() {
			return ICL_LANGUAGE_CODE;
		});
//echo esc_url($image['url'])
if( !empty( $img ) ): ?>
    <img class="city-img" src="<?php echo esc_url($img['url']); ?>"  />


<?php else: ?>



	
                            
                                <video loop autoplay muted preload="auto" id="bgvid">
                                    <source src="/wp-content/uploads/Sequenza-per-sfondo-sito_constant_1080.mp4" type="video/mp4" poster="/poster1.jpg">
                                    <?php _e( 'Your browser does not support the video tag.', 'Parma' ); ?>
                                </video>
 <?php endif; ?>
                               
                                
                            </section>
                            <?php  if(ICL_LANGUAGE_CODE=='it'): ?>
							

                            <a data-fancybox data-width="100%" data-height="680" id="p6" href="/wp-content/uploads/Spot-2-x-WEB-Standard.mp4" style="z-index: 890;
    position: absolute;">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play.svg" class="vid-play">
                            </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                    
     											<?php // WP_Query arguments
$args = array(
	'post_type'              => array( 'news' ),
	'posts_per_page' => 2,
);
$digi_artcl = new WP_Query( $args );
$num = $digi_artcl->post_count; 

?>
                    				<div class=" type-txt-small-sans text-below">
										<div class="columns ">
						<div class=" main-txt">
							<div class="columns  ">


							<?php if ($num>0) : ?>

								<div class="big-video2 notizie full-w ">
									<div class=" home-news-banner large-offset-2  large-9">
									<div class="columns  title-notizie"><?php _e( 'Notizie più recenti.', 'Parma' ); ?>	</div>									 

										<?php 
											if ($num>2){
											$translated_page = icl_object_id(792, 'page', true);
											echo '
											<a href="'.get_permalink( $translated_page ).'">
										<div class="all-notizie">'. _e( 'Leggi tutte', 'Parma' ).' </div>	</a>';
										}		
										?>						 
									<div class="row column news-cols">
									
									
											
											<?php // WP_Query arguments


// The Query

// The Loop
if ( $digi_artcl->have_posts() ) {
	while ( $digi_artcl->have_posts() ) {
		$digi_artcl->the_post();
		
		
				echo'	
									<div class="medium-6 columns home-news-item">

											
											<h3>'.get_field("news_date").'</h3> 
											<h1>
							'.get_the_title().':  </h1>
											<p>'.wp_trim_words( get_the_content(), 40, '...' ).'</p> 
										</div>
									';
										}
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();
?>
						
									
	 								
									
									
									
									
									
									</div>
									</div>
									
									
									</div>
									
									<?php									endif; ?>

								<div class="search-div  large-offset-2  large-9">	
									
									<div class=" titolo-contributi">
										<?php _e( 'I vostri contributi', 'Parma' ); ?> 
</div>


									
									
									<div id="home-loop">
										
																				
<?php  
	
	 
	if(ICL_LANGUAGE_CODE=='es'): 
	echo do_shortcode( '[gmw_ajax_form form="7"]' ); 
 elseif(ICL_LANGUAGE_CODE=='it'): 
	echo do_shortcode( '[gmw_ajax_form form="2"]' ); 

elseif(ICL_LANGUAGE_CODE=='de'): 
	echo do_shortcode( '[gmw_ajax_form form="6"]' ); 
elseif(ICL_LANGUAGE_CODE=='fr'): 
	echo do_shortcode( '[gmw_ajax_form form="8"]' ); 
elseif(ICL_LANGUAGE_CODE=='sv'): 
	echo do_shortcode( '[gmw_ajax_form form="9"]' ); 
elseif(ICL_LANGUAGE_CODE=='hr'): 
	echo do_shortcode( '[gmw_ajax_form form="10"]' ); 


endif; ?>
	
	
	


	 </div> <!-- /Home-loop-->


										
																			<?php //echo do_shortcode( '[searchandfilter id="580" show="results"]' ); ?>

					
</div>
								</div>
								<div class="type-txt-big-sans">
									


</div>
							
						</div>

						<div class="row">
							<div class="columns medium-12 large-12">
								<p></p>
							</div>
							
							
							

						</div>                                
                                
                                
                                
                                
                                                                <div class="row type-txt-small-sans partner" id="partner">
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>




<?php get_footer('577'); ?>