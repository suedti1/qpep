<!-- End Header and Nav -->
<!-- First Band (Image) -->
<!-- Second Band (Image Left with Text) -->
<!-- Third Band (Image Right with Text) -->
<!-- Footer -->

<html>
<head>
    <title></title>
</head>

<body>
    <div class="row faq-row">
        <div class="columns medium-12 large-12">
            <div>
                <div class="faq-cont"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/faq.svg" class="faq"></div>
            </div>
        </div>
    </div>

    <footer class="row">
        <div class="large-12 columns ll">
            <!--
                    <div class="row">
                        <div class="large-6 columns">
</div>
                        <div class="large-6 columns">
                            <ul class="float-right menu footer-links">
                                <li>
                                    <a href="#"><?php _e( '© Comune di Parma 2018', 'Parma' ); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php _e( 'P.iva. 00000000', 'Parma' ); ?> </a>
                                </li>
                                <li>
                                    <a href="#"><?php _e( 'Privacy & Cookies', 'Parma' ); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php _e( 'Contatti & Imprint', 'Parma' ); ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>-->
            <?php if ( is_front_page() ) : 
                                                                     
                                                                 ?>

            <nav class="nav fixed-nav">
                <div class="row nav-sticky">
                    <div class="large-12 columns small-12 slogan-cont2">
                        <div class="slog-cont">
                            <?php //if ( is_front_page() ) : 
                                                                                     
                                                                                 ?> <span class="type-xs-txt-big-sans mob-slogan"><?php _e( 'Qui prima o poi', 'Parma' ); ?></span> <span class="type-xs-txt-big-sans desk-slogan"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logline.svg" class="slogan-home-header"></span> <a href="/proposta/" class="btn-proposta"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/btn_partecipa_top.svg" style="float: right; margin-top: -45px;"></a> <?php 
                                                                                //else: 
                                                                                ?> <?php //endif; 
                                                                                
                                                                            ?>
                        </div>
                    </div>

                    <div class="medium-4 columns small-4">
                        <!--<img src="img/menu.svg" class="btn-menu"/>-->
                    </div>
                </div>
            </nav><?php endif; 
                                                                
                                                            ?>
        </div>
    </footer><script type="text/javascript">
/*
        var vid = document.getElementById("bgvid");
        var pauseButton = document.querySelector("#");

        function vidFade() {
          vid.classList.add("stopfade");
        }

        vid.addEventListener('ended', function()
        {
        // only functional if "loop" is removed
        vid.pause();
        // to capture IE10
        vidFade();
        });


        pauseButton.addEventListener("click", function() {
          vid.classList.toggle("stopfade");
          if (vid.paused) {
            vid.play();
            pauseButton.innerHTML = "Pause";
          } else {
            vid.pause();
            pauseButton.innerHTML = "Paused";
          }
        });


    $(document).on("scroll", function() {
    if ($(document).scrollTop() > 50) {
    $(".pagewrapper").removeClass("scrolltop").addClass("scrolldown");
    $(".nav-placeholder").height($(".nav").outerHeight());
    console.log($(".nav").outerHeight());
    } else {
    $(".pagewrapper").removeClass("scrolldown").addClass("scrolltop");
    $(".nav-placeholder").height(0);
    }
    });
    */
    jQuery( document ).ready( function( $ ) {

    $(window).scroll(function() {
    if ($(this).scrollTop() > 100){

    $('nav').addClass("sticky");


    }
    else{$('nav').removeClass("sticky");


    }
    });

                //faq menu
        jQuery(".faq").click(function(event) {
              event.preventDefault();

    if (jQuery('#lowerC').css('display') == 'block') {
        var height = '-=' + jQuery('.lowerC').height();
    } else {
        var height = '+=' + jQuery('.lowerC').height();
    }
    //jQuery("#panel").slideToggle(2000);


    // $("#panel").toggleClass("panelUP",2000);
    $(".lowerC").toggleClass("lowerUP",2000);


    // jQuery("#upper").animate({
    //      bottom: height
    //  }, 4000)
    });



    } );


    </script><script type="text/javascript">
  jQuery(document).foundation();
    </script><!--pagewrapper -->

    <div class="lowerC">
        <div>
            <div class="row">
                <div class="large-offset-3">
                    <h2 class="faq-title"><?php _e( 'FAQ', 'Parma' ); ?></h2>
                </div>
            </div>

            <div class="row faq-txt">
	                            <div class="large-4 columns large-offset-2 faq-txt-cont">

                <?php
	                $this_page=70;
	                
	                if(have_rows('faq', $this_page)):
						while(have_rows('faq', $this_page)): the_row();
							if( get_row_layout() == 'faq2' ):
	          
                    $this_text = get_sub_field('title');
					//$this_text_plain = strip_tags($this_text);
                    
                    
                    $sub_testo=get_sub_field('answer');
                    
                    echo '<h3 class="type-txt-bold-small-sans">'. $this_text.'</h3>';
                    echo ' <div class="type-txt-small-serif">'.$sub_testo.'</div>';
                    
                    endif;
      endwhile;
endif;
                                            ?>
								</div>			

              

                <div class="large-4 columns  end">

                <?php
	                $this_page=72;
	                
	                if(have_rows('faq', $this_page)):
						while(have_rows('faq', $this_page)): the_row();
							if( get_row_layout() == 'faq2' ):
	          
                    $this_text = get_sub_field('title');
					//$this_text_plain = strip_tags($this_text);
                    
                    
                    $sub_testo=get_sub_field('answer');
                    
                    echo '<h3 class="type-txt-bold-small-sans">'. $this_text.'</h3>';
                    echo ' <div class="type-txt-small-serif">'.$sub_testo.'</div>';
                    
                    endif;
      endwhile;
endif;
                                            ?>

                   
                                   </div><br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div><script type="text/javascript">
<?php if ( is_front_page() ) : 
                                                         
                                                     ?>     
       jQuery( document ).ready( function( $ ) {

    jQuery.fn.getCoord = function()
    {
    var elem = $(this);
    var x = elem.offset().left;
    var y = elem.offset().top;
    //console.log('x: ' + x + ' y: ' + y);
    //output: x: 70 y: 40

    return {  
      "x" : x, 
      "y" : y 
    };

    //note that it is not efficient as it breaks the jQuery chain
    //return elem;
    };

    // Function To Draw To The Canvas

    a=5;
    function draw(scroller) {
    // Clear Canvas At The Start Of Every Frame
    console.log('drww');

    if (scroller>=240){
                $('.btn-partecipa').css("z-index","899");

    }
     else{
                        $('.btn-partecipa').css("z-index","1200");

    } 
     
    viewportH=    $(window).height();   // returns height of browser viewport
    viewportW= $(window).width();

     var canvasElement = document.querySelector("#myCanvas");
    var context = canvasElement.getContext("2d");


    var x = $("body").position(); 
    var x = $('.qui').offset();
        //console.log("Top position: " + x.top + " Left position: " + x.right);
        
        $('#myCanvas').parent().parent().getCoord();


    // the triangle

    /*context.beginPath();
    context.moveTo(100, 0);
    context.lineTo(300, 0);
    context.lineTo(400, 300);
    context.closePath();

    */

     context.clearRect(0,0,10,10); 


    // context.moveTo(70, 10); 

    // Draw Additional Randomly Placed Lines
    //    for (var i = 0; i < 700; i++) {
    //      var ab=a+i;
    //      console.log(ab);
    pos=parseInt(scroller/3);
    left_start=50;
    top_start=0;

    ppos=100+pos;
    pppos=parseInt(pos*1.49);

    //console.log('y:'+ppos+'qui:'+pppos+'pos:'+pos+', '+$(this).scrollTop()+', H:'+viewportH+', scroll:'+x.top);

          // context.moveTo(70+scroller, 100); 


    if (pos<=viewportH-250){
              context.clearRect(0, 0, context.canvas.width, context.canvas.height);

           context.beginPath(); 
    context.moveTo(left_start, 0+pos);  
    context.lineTo(550, 0);
    context.lineTo(800, 600); 
    context.closePath();
     context.clearRect(0,0,100,100); 






qui_scroll=1.34;

     if(pos<-444){
     qui_scroll=1;
	 }
    //$(".qui").css('top',parseInt(pos*1.34));
    
    
    
    
    $(".qui").css('top',parseInt(pos*qui_scroll));
    
    //  }


    ///$(".qui").css('top',parseInt(pos*1.34));
    
    //  }

    context.fillStyle = "#ffe500c9";      
    context.fill(); 

        
    }



    // Call Draw Function Again To Continue 
    // Drawing To Canvas To Create Animation
    // window.requestAnimationFrame(draw);
    }

    // Initialize The Draw Function
    $(window).scroll(function() {


    if ($(this).scrollTop() > 1)

    draw($(this).scrollTop());

    });


    window.onload = function () {
    fr=$(this).scrollTop();
    draw($(this).scrollTop());

    setTimeout(draw(fr), 500);


    }






    } );
    <?php endif; 
                                                    
                                                ?>
    </script><?php wp_footer(); ?>
</body>
</html>
