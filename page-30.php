               <?php
get_header(); ?>

                <div class="row full-w">
                    <div class="large-12 columns full-w-c">
                        <div class="wrapper">
	                        <!--
                            <a data-fancybox data-type="iframe" data-src="/proposta/" href="javascript:;"> -->
                            <a href="/?page_id=2/" >
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/partecipa.svg" class="float-center above-video btn-partecipa tr">
                            </a>
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logline-2.svg" class="slogan-home" id="p7"/>
                          
                            <section id="big-video"><a id="video">
                                <video loop autoplay muted preload="auto" id="bgvid">
                                    <source src="/wp-content/uploads/Sequenza-per-sfondo-sito_constant_1080.mp4" type="video/mp4" poster="/poster1.jpg">
                                    <?php _e( 'Your browser does not support the video tag.', 'Parma' ); ?>
                                </video>
                            </section>
                            <a data-fancybox data-width="100%" data-height="680" id="p6" href="/wp-content/uploads/Spot-2-x-WEB-Standard.mp4" style="z-index: 901;
    position: relative;">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play.svg" class="vid-play">
                            </a>
                        </div>
                    </div>
                </div>
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="row type-txt-small-sans text-below">
                            <div class="columns large-2">
</div>
                            <div class="columns large-10 small-offset-2">
                                <div class="row main-txt">
                                    <div class=" columns large-7 medium-7 small-9 small-offset-2 large-offset-0">
                                <h4 class="type-txt-small-sans"><?php _e( 'Esplora il progetto Qui prima e poi', 'Parma' ); ?><a id="esplora" ></a></h4>
                                                                            
<div class="type-txt-big-sans"><?php the_field('p1', 'option'); ?></div>
                                    </div>
                                </div>
                                                            <a data-fancybox data-width="100%" data-height="680" href="/P_video_lungo_1080p-LoRe.mp4" style="z-index: 901;
    position: relative;" class="small-offset-2  large-offset-0">
                          
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play-2.svg" class="vod-play2" />
                                                            </a>
                                <div class="row">
                                    <div class="columns medium-12 large-12">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans ">
                                    <div class="columns large-5 credits-col   medium-7 small-9 small-offset-2  large-offset-0 ">
                                        <h4 class="type-txt-small-sans"><?php _e( 'Credits', 'Parma' ); ?></h4>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans credits-row">
                                    <div class="columns large-5 medium-7 small-9 small-offset-2  large-offset-0">
                                                                               <div class="type-txt-big-sans field-p2"><?php the_field('p2', 'option'); ?></div>

                                    </div>
                                </div>
                                <div class="row type-txt-small-sans partner" id="partner">
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>

<?php get_footer(); ?>