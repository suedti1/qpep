<?php
get_header(); ?>

                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="row type-txt-small-sans text-below">
                            <div class="columns large-2">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <div class="columns large-8">
                                <?php the_content(); ?>
                            </div>
                                                        <div class="columns large-2">
</div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>

<?php get_footer(); ?>