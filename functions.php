                                                                                                                               <?php
if ( ! function_exists( 'fgh_setup' ) ) :

function fgh_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     */
    /* Pinegrow generated Load Text Domain Begin */
    load_theme_textdomain( 'Parma', get_template_directory() . '/languages' );
    /* Pinegrow generated Load Text Domain End */

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     */
    add_theme_support( 'title-tag' );
    
    /*
     * Enable support for Post Thumbnails on posts and pages.
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

    // Add menus.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'fgh' ),
        'social'  => __( 'Social Links Menu', 'fgh' ),
    ) );

    /* 
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    /*
     * Enable support for Post Formats.
     */
    add_theme_support( 'post-formats', array(
        'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
    ) );
}
endif; // fgh_setup

add_action( 'after_setup_theme', 'fgh_setup' );


if ( ! function_exists( 'fgh_init' ) ) :

function fgh_init() {

    
    // Use categories and tags with attachments
    register_taxonomy_for_object_type( 'category', 'attachment' );
    register_taxonomy_for_object_type( 'post_tag', 'attachment' );

    /*
     * Register custom post types. You can also move this code to a plugin.
     */
    /* Pinegrow generated Custom Post Types Begin */

    /* Pinegrow generated Custom Post Types End */
    
    /*
     * Register custom taxonomies. You can also move this code to a plugin.
     */
    /* Pinegrow generated Taxonomies Begin */

    /* Pinegrow generated Taxonomies End */

}
endif; // fgh_setup

add_action( 'init', 'fgh_init' );


if ( ! function_exists( 'fgh_widgets_init' ) ) :

function fgh_widgets_init() {

    /*
     * Register widget areas.
     */
    /* Pinegrow generated Register Sidebars Begin */

    /* Pinegrow generated Register Sidebars End */
}
add_action( 'widgets_init', 'fgh_widgets_init' );
endif;// fgh_widgets_init



if ( ! function_exists( 'fgh_customize_register' ) ) :

function fgh_customize_register( $wp_customize ) {
    // Do stuff with $wp_customize, the WP_Customize_Manager object.

    /* Pinegrow generated Customizer Controls Begin */

    /* Pinegrow generated Customizer Controls End */

}
add_action( 'customize_register', 'fgh_customize_register' );
endif;// fgh_customize_register


if ( ! function_exists( 'fgh_enqueue_scripts' ) ) :
    function fgh_enqueue_scripts() {

        /* Pinegrow generated Enqueue Scripts Begin */

    wp_deregister_script( 'modernizr' );
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr.js', false, null, false);

    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/vendor/jquery.min.js', false, null, false);

    wp_deregister_script( 'foundation' );
    wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/foundation.min.js', false, null, false);

    wp_deregister_script( 'jqueryfancybox' );
    wp_enqueue_script( 'jqueryfancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js', false, null, false);


    wp_deregister_script( 'colorb' );
    wp_enqueue_script( 'colorb', get_template_directory_uri() . '/js/jquery.colorbox-min.js', false, null, false);



    wp_deregister_script( 'jqueryroyal' );
    wp_enqueue_script( 'jqueryroyal', get_template_directory_uri() . '/js/jquery.royalslider.min.js', false, null, false);


    /* Pinegrow generated Enqueue Scripts End */

        /* Pinegrow generated Enqueue Styles Begin */

    wp_deregister_style( 'foundation' );
    wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.css', false, null, 'all');

    wp_deregister_style( 'style' );
    wp_enqueue_style( 'style', get_bloginfo('stylesheet_url'), false, null, 'all');

    wp_deregister_style( 'jqueryfancybox' );
    wp_enqueue_style( 'jqueryfancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css', false, null, 'all');

    wp_deregister_style( 'style-1' );
    wp_enqueue_style( 'style-1', get_template_directory_uri() . '/custom.scss', false, null, 'all');

    wp_deregister_style( 'typexstylesheet' );
    wp_enqueue_style( 'typexstylesheet', get_template_directory_uri() . '/typex-stylesheet4.css', false, null, 'all');

    wp_deregister_style( 'fsnavbar' );
    wp_enqueue_style( 'fsnavbar', get_template_directory_uri() . '/components/pg.fs-navbar/css/fs-navbar-1.css', false, null, 'all');

   wp_deregister_style( 'royalCSSMain' );
    wp_enqueue_style( 'royalCSSMain', get_template_directory_uri() . '/css/royalslider.css', false, null, 'all');

   wp_deregister_style( 'royalCSS' );
    wp_enqueue_style( 'royalCSS', get_template_directory_uri() . '/css/rs-default.css', false, null, 'all');

    /* Pinegrow generated Enqueue Styles End */

    }
    add_action( 'wp_enqueue_scripts', 'fgh_enqueue_scripts' );
endif;

function pgwp_sanitize_placeholder($input) { return $input; }
/*
 * Resource files included by Pinegrow.
 */
/* Pinegrow generated Include Resources Begin */

    /* Pinegrow generated Include Resources End */


//CUSTOM CODE
		  //  wp_deregister_script( 'jquery' );
 //   wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js?ver=1.12.4', false, null, false);

   //   wp_enqueue_script( 'migrate', '/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1', false, null, false);

function wpdocs_theme_name_scripts() { 
   wp_enqueue_script( 'migrate', '/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1', false, null, false);
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );





function register_my_menu() {
  register_nav_menu('new-menu',__( 'New Menu' ));
}
add_action( 'init', 'register_my_menu' );

/**
 * ACF Options Page 
 *
 */
function ea_acf_options_page() {
    if ( function_exists( 'acf_add_options_page' ) ) {
        acf_add_options_page( array( 
        	'title'      => 'Site Options',
        	'capability' => 'manage_options',
        ) );
    }
}
add_action( 'init', 'ea_acf_options_page' );



//Save first image of gallery as featured image 
add_action( 'save_post', 'set_featured_image_from_gallery' );

function set_featured_image_from_gallery() {
  $has_thumbnail = get_the_post_thumbnail($post->ID);

  if ( !$has_thumbnail ) {

    $images = get_field('file', false, false);
    $image_id = $images[0];

    if ( $image_id ) {
      set_post_thumbnail( $post->ID, $image_id );
    }
  }
}




/**
 * Create the image attachment and return the new media upload id.
 *
 * @author Joshua David Nelson, josh@joshuadnelson.com
 *
 * @since 03.29.2017 updated to a class, utilizing code from Takuro Hishikawa's gist linked below.
 *
 * @see https://gist.github.com/hissy/7352933
 * 
 * @see http://codex.wordpress.org/Function_Reference/wp_insert_attachment#Example
 * 
 * @link https://joshuadnelson.com/programmatically-add-images-to-media-library/
 */
class JDN_Create_Media_File {
	
	var $post_id;
	var $image_url;
	var $wp_upload_url;
	var $attachment_id;
	
	/**
	 * Setup the class variables
	 */
	public function __construct( $image_url, $post_id = 0 ) {
		
		// Setup class variables
		$this->image_url = esc_url( $image_url );
		$this->post_id = absint( $post_id );
		$this->wp_upload_url = $this->get_wp_upload_url();
		$this->attachment_id = $this->attachment_id ?: false;
		
		return $this->create_image_id();
		
	}
	
	/**
	 * Set the upload directory
	 */
	private function get_wp_upload_url() {
		$wp_upload_dir = wp_upload_dir();
		return isset( $wp_upload_dir['url'] ) ? $wp_upload_dir['url'] : false;
	}
	
	/**
	 * Create the image and return the new media upload id.
	 *
	 * @see https://gist.github.com/hissy/7352933
	 *
	 * @see http://codex.wordpress.org/Function_Reference/wp_insert_attachment#Example
	 */
	public function create_image_id() {
		
		if( $this->attachment_id )
			return $this->attachment_id;
		
		if( empty( $this->image_url ) || empty( $this->wp_upload_url ) )
			return false;
		
		$filename = basename( $this->image_url );
		
		$upload_file = wp_upload_bits( $filename, null, file_get_contents( $this->image_url ) );
		
		if ( ! $upload_file['error'] ) {
			$wp_filetype = wp_check_filetype( $filename, null );
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_parent' => $this->post_id,
				'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
				'post_content' => '',
				'post_status' => 'inherit'
			);
			$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $this->post_id );
			
			if( ! is_wp_error( $attachment_id ) ) {
				
				require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
				
				$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
				wp_update_attachment_metadata( $attachment_id,  $attachment_data );
				
				$this->attachment_id = $attachment_id;
				
				return $attachment_id;
			}
		}
		
		return false;
		
	} // end function get_image_id
}

// Example Usage

// Add an unattached image to the media library

/*
$image_url = 'http://someimageurl';
$create_image = new JDN_Create_Media_File( $image_url );
$image_id = $create_image->attachment_id;
*/

// Add image and attach to a post
/*
$post_id = '3'; // the id of the post
$create_image = new JDN_Create_Media_File( $image_url, $post_id );
$image_id = $create_image->attachment_id;
*/




function remove_core_updates(){
  //  global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates');
//add_filter('pre_site_transient_update_plugins','remove_core_updates');
add_filter('pre_site_transient_update_themes','remove_core_updates');

function post_remove ()      //creating functions post_remove for removing menu item
{ 
   //remove_menu_page('edit.php');
    //  remove_menu_page( 'edit.php?post_type=page' );
      remove_menu_page( 'edit-comments.php' );
      remove_menu_page( 'tools.php' );
      remove_menu_page( 'about.php' );
  

}

add_action('admin_menu', 'post_remove');

 //hide gravity in admin
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {

if(current_user_can('editor')) {
  echo '<style>
    .toplevel_page_gf_edit_forms,#wp-admin-bar-gform-forms,#wp-admin-bar-wpseo-menu {
      display:none !important;
    } 
    
  </style>';
  }
  }
add_action('admin_head', 'my_custom_fonts');



/* ADD custom field for city to taxonomy on SAVE*/

add_action( 'save_post', 'add_video_taxonomy' );

function add_video_taxonomy( $post_id ) {

    // you should check if the current user can do this, probably against publish_posts
    // you should also have a nonce check here as well

    
    $meta_value=get_post_meta(get_the_ID(),'luogo',true);
            wp_set_post_terms( $post_id, $meta_value, 'city', true );
  

}


/* ADD custom field for city to taxonomy on GRAVIY SUBMIT */


add_action( 'gform_after_submission', 'set_post_content', 10, 2 );      
function set_post_content( $entry, $form ) {
 
    //getting post
    $post = get_post( $entry['post_id'] );
 
     $meta_value=get_post_meta($entry['post_id'],'luogo',true);

   //changing post content
    //$post->post_content = 'Blender Version:' . rgar( $entry, '7' ) . "<br/> <img src='" . rgar( $entry, '8' ) . "'> <br/> <br/> " . rgar( $entry, '13' ) . " <br/> <img src='" . rgar( $entry, '5' ) . "'>";
 
    //updating post
    //wp_update_post( $post );
            wp_set_post_terms( $entry['post_id'], $meta_value, 'city', true );

}



add_filter( ‘gform_notification_5’, ‘route_notification’, 10, 3);

function route_notification($notification, $form , $entry) {

global $post;

$email_to = get_post_meta($post->ID, ’emailaddress’, true);

if ($email_to){

$notification[‘to’] = $email_to;

}

return $notification ;

}





function gfgeo_change_locator_button_language( $label ) {
 
	// if 'en' exists in URL display label in English
	if ( strpos( $_SERVER['REQUEST_URI'], 'fr' ) ) {
		
		// modify the language to fr
 		$label = 'Signaler ma position actuelle';

	// otherwise, display label in en
	} else {
 
		$label = 'Signal my current position';
	}
 
	return $label;
}
add_filter( 'gfgeo_locator_button_label', 'gfgeo_change_locator_button_language' );

function carapace_change_map_js_lang($maps_api_args) {
    
    $locale = get_locale();
    $my_current_lang = apply_filters( 'wpml_current_language', NULL );

  //  if ( $locale == "en_US" ) {
        $maps_api_args['language'] = $my_current_lang;
   // }

    return $maps_api_args;
}
add_filter('gmw_google_maps_api_args', 'carapace_change_map_js_lang');


add_action('gform_export_separator', 'tab_separator');
function tab_separator($sep) {
    return "\t";
}

?> 