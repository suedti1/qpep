<?php
get_header(); ?>

                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="row type-txt-small-sans text-below">
                            <div class="columns large-2">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <div class="columns large-8">

<li id="gmw-single-post-<?php echo esc_attr( $post->ID ); ?>" class="single-content">


		  <div class="single-loop-item">	

										<div class="taxonomies">	
										
										<?php 

    $fueltype = wp_get_post_terms(get_the_id(  ), 'category');

foreach( $fueltype as $term ) {
    // Get the term link
    $term_link = get_term_link( $term );

   
    echo '<span class="bttn cat">'. $term->name .'</span> ';
    		
} 


    $loc = wp_get_post_terms(get_the_id(  ), 'city');

foreach( $loc as $term ) {
    // Get the term link
    $term_link = get_term_link( $term );

   
    echo '<span class="bttn location">'. $term->name .'</span> ';
    		
} 

?>
											<div class="details-popup"></div>

											
											
										</div>
											<div class="medium-8 columns loop-item">

											<h1><?php the_title(); ?></h1> 
											<p><?php the_content(); ?></p>
											</div>		
											<div class="medium-4 columns loop-item">
																					
						<div class="img-list-img">					
            

 <?php 
$images = get_field('file');

    if( $images ): 
        $image_1 = $images[0]; 
?>                
	            
                <img src="<?php echo $images[0]['sizes']['home-list']; ?>"  alt="<?php echo $image['alt']; ?>" />
           
<?php endif; ?>
				
			 							
											</div>	
											</div>

<div class="break"></div>	
											<div class="add-info row columns medium-12">
												<?php _e( 'Informazioni aggiuntive', 'Parma' );	?>	
											</div>
											<div class="add-info2">
											
												<div class="medium-3 columns detail detail1">
													<h3><?php _e( 'Categoria', 'Parma' );	?>	</h3>
													<?php 
														    $fueltype = wp_get_post_terms(get_the_id(  ), 'category');

foreach( $fueltype as $term ) {
    // Get the term link
    $term_link = get_term_link( $term );

   
    echo  $term->name .'';
    		
} 
														
													?>

												</div>
												<div class="medium-3 columns detail">
													<h3><?php _e( 'Comune', 'Parma' );	?>	</h3>
													<?php 
														
														
    $loc = wp_get_post_terms(get_the_id(  ), 'city');

foreach( $loc as $term ) {
    // Get the term link
    $term_link = get_term_link( $term );

   
    echo '<span class=" location">'. $term->name .'</span> ';
    }
													?>
													

												</div>
												<?php if (get_field( 'publication_of_name' )!=1){ $cclass="detail-last";} else{ $cclass="";} ?>

												<div class="medium-3 columns detail <?php echo $cclass; ?>">
													<h3><?php _e( 'Indirizzo', 'Parma' );	?>	</h3>
													<?php the_field( 'long' ); ?>
													
												</div> 
												<?php if (get_field( 'publication_of_name' )==1): ?>

												<div class="medium-3 columns detail detail-last">
													<h3><?php _e( 'Inviato da', 'Parma' );	?>	</h3>
													<?php 
														
														the_field( 'nome' ); $fr= get_field( 'cognome' );?>
													<?php echo ' '.substr($fr, 0, 99); 
														
															
															
															
															?>
												</div>
												<?php endif; ?> 
												
														
											</div>	
											
										<div class="medium-1 columns">
											&nbsp;
										</div>
												
										<div class="medium-2 columns">
											
											

											
											
											
													
										</div>
										<div class="medium-1 columns">
											
										</div>
																			<div class="break">	</div>

									  </div>




                               <?php echo do_shortcode( '[gmw_single_location]' ); ?>

                            </div>
                                                        <div class="columns large-2">
</div>

<div class="columns large-12">
	
	<div class="gal ">
	                        <?php 

$images = get_field('file');

if( $images ): ?>
              <div id="gallery-1" class="royalSlider rsDefault visibleNearby">
        <?php foreach( $images as $image ): ?>
        
  
                <a href="<?php echo $image['sizes']['medium_large']; ?>" class="rsImg" data-rsw="<?php echo $image['sizes']['medium_large-width']; ?>" data-rsh="<?php echo $image['sizes']['medium_large-height']; ?>"> <?php echo $image['alt']; ?> </a>

        <?php endforeach; ?>
  </div><?php endif; ?>
	                        
                        </div>	
</div>

                        
                        </div>
                        

                        
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>
                
  <script>/*

                    jQuery( document ).ready( function( $ ) {

	 jQuery('#cclose').click(function(){
            parent.$.colorbox.close();
            return false;
        });
})
*/
</script>
<?php get_footer(); ?>