               <?php
get_header(); ?>

                <div class="row full-w">
                    <div class="large-12 columns full-w-c">
                        <div class="wrapper">
	                        <!--
                            <a data-fancybox data-type="iframe" data-src="/proposta/" href="javascript:;"> -->
                            <a href="/?page_id=2/" >
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/partecipa.svg" class="float-center above-video btn-partecipa tr">
                            </a>
                           <!-- <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logline-2.svg" class="slogan-home" id="p7"/> -->
                          
                            <section id="big-video"><a id="video">
                                <video loop autoplay muted preload="auto" id="bgvid">
                                    <source src="/wp-content/uploads/Sequenza-per-sfondo-sito_constant_1080.mp4" type="video/mp4" poster="/poster1.jpg">
                                    <?php _e( 'Your browser does not support the video tag.', 'Parma' ); ?>
                                </video>
                            </section>
                            <a data-fancybox data-width="100%" data-height="680" id="p6" href="/wp-content/uploads/Spot-2-x-WEB-Standard.mp4" style="z-index: 901;
    position: absolute;">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play.svg" class="vid-play">
                            </a>
                        </div>
                    </div>
                </div>
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                    
                    				<div class=" type-txt-small-sans text-below">
										<div class="columns ">
						<div class=" main-txt">
							<div class="columns  ">
								<div class="big-video2 notizie full-w ">
									<div class=" home-news-banner large-offset-2 medium-offset-2 large-9">
									<div class="columns  title-notizie">	Notizie più recenti</div>									 
									<div class="all-notizie"> Leggi tutte</div>									 
									<div class="row column news-cols">
										<div class="medium-6 columns home-news-item">
											<h3>03 luglio 2019</h3> 
											<h1>
							Incontro con i responsabili del progetto presso il Comune di Parma  </h1>
											<p>Venerdì, 5 luglio alle ore 15 presso la sala riunioni del Comune di Parma si terrà l’incontro…</p> 
										</div>
										<div class="medium-6 columns home-news-item">
											<h3>24 giugno 2019</h3> 
											<h1>
							Conferenza stampa per la presentazione del progetto Qui prima e poi  </h1>
											<p>Sabato, 29 giugno è stata indetta una conferenza stampa a cui sono invitati a partecipare tutti…</p> 
										</div>
									</div>
									</div>
									</div>
								<div class="search-div  large-offset-2 medium-offset-2 large-9">	
									
									<div class=" titolo-contributi">
										I vostri contributi
</div>
									<div class=" filtra-contributi">
										Filtra i contenuti per categoria
										<?php echo do_shortcode( '[searchandfilter id="580"]' ); ?>
</div>


										
																			<?php echo do_shortcode( '[searchandfilter id="580" show="results"]' ); ?>

					
</div>
								</div>
								<div class="type-txt-big-sans">
</div>
							
						</div>

						<div class="row">
							<div class="columns medium-12 large-12">
								<p></p>
							</div>
							
							
							
	<div class=" header-results">
										<div class="medium-6 columns vista">
											Vista griglia				
</div>
										<div class="medium-6 columns vista">
											Vista mappa			
</div>
									</div>
									
									
									<div id="home-loop">
										
<?php  
	
	 
	if(ICL_LANGUAGE_CODE=='es'): 
	echo do_shortcode( '[gmw_ajax_form form="4"]' ); 
 elseif(ICL_LANGUAGE_CODE=='it'): 
	echo do_shortcode( '[gmw_ajax_form form="2"]' ); 
endif; ?>
	
	
	
?>

	 </div> <!-- /Home-loop-->

						</div>                                
                                
                                
                                
                                
                                
                                <div class="row type-txt-small-sans ">
                                    <div class="columns large-5 credits-col   medium-7 small-9 small-offset-2  large-offset-0 ">
                                        <h4 class="type-txt-small-sans"><?php _e( 'Credits', 'Parma' ); ?></h4>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans credits-row">
                                    <div class="columns large-5 medium-7 small-9 small-offset-2  large-offset-0">
                                                                               <div class="type-txt-big-sans field-p2"><?php the_field('p2', 'option'); ?></div>

                                    </div>
                                </div>
                                <div class="row type-txt-small-sans partner" id="partner">
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>

<?php get_footer(); ?>