<?php
/**
 * GEO my WP Posts Locator Ajax Form Class.
 *
 * The class queries posts based on location.
 *
 * @package gmw-ajax-forms
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 *
 * GMW_Posts_Locator_Ajax_Form class
 *
 * @since 1.0
 *
 * @author Eyal Fitoussi
 */
class GMW_Posts_Locator_Ajax_Form extends GMW_Ajax_Form {

	/**
	 * Object type.
	 *
	 * @var string
	 */
	public $object_type = 'post';

	/**
	 * Database columns.
	 *
	 * @var array
	 */
	public $db_fields = array(
		'gmw_locations.object_type',
		'gmw_locations.object_id',
		'gmw_locations.featured as featured_location',
		'gmw_locations.ID as location_id',
		'gmw_locations.latitude as lat',
		'gmw_locations.longitude as lng',
		'gmw_locations.street',
		'gmw_locations.neighborhood',
		'gmw_locations.city',
		'gmw_locations.region_code',
		'gmw_locations.region_name',
		'gmw_locations.country_code',
		'gmw_locations.country_name',
		'gmw_locations.postcode',
		'gmw_locations.address',
		'gmw_locations.formatted_address',
	);

	/**
	 * Query clauses
	 *
	 * @since 1.0
	 *
	 * @param  array $clauses array of query clauses.
	 *
	 * @return modified $clauses
	 */
	public function query_clauses( $clauses ) {

		global $wpdb;

		// prepare db fields.
		$this->db_fields = implode( ',', $this->db_fields );

		// add the location db fields to the query.
		if ( '' !== $this->form['results_enabled'] ) {
			$clauses['fields'] .= ", {$this->db_fields}";
		} else {
			$clauses['fields'] = "{$wpdb->posts}.ID, {$wpdb->posts}.post_parent, " . $this->db_fields;
		}

		$clauses['having'] = '';

		// In multisite we need to check for the blog ID.
		if ( is_multisite() && ! empty( $wpdb->blogid ) ) {
			$blog_id           = absint( $wpdb->blogid );
			$clauses['where'] .= " AND gmw_locations.blog_id = {$blog_id} ";
		}

		// get address filters query.
		$address_filters = GMW_Location::query_address_fields( $this->get_address_filters(), $this->form );

		// when address provided, and not filtering based on address fields, we will do proximity search.
		if ( '' === $address_filters && ! empty( $this->form['lat'] ) && ! empty( $this->form['lng'] ) ) {

			// generate some radius/units data.
			if ( in_array( $this->form['units'], array( 'imperial', 3959, 'miles', '3959' ), true ) ) {
				$earth_radius = 3959;
				$units        = 'mi';
				$degree       = 69.0;
			} else {
				$earth_radius = 6371;
				$units        = 'km';
				$degree       = 111.045;
			}

			// add units to locations data.
			$clauses['fields'] .= ", '{$units}' AS units ";

			// since these values are repeatable, we escape them previous
			// the query instead of running multiple prepares.
			$lat      = esc_sql( $this->form['lat'] );
			$lng      = esc_sql( $this->form['lng'] );
			$distance = ! empty( $this->form['radius'] ) ? esc_sql( $this->form['radius'] ) : '';

			$clauses['fields'] .= ", ROUND( {$earth_radius} * acos( cos( radians( {$lat} ) ) * cos( radians( gmw_locations.latitude ) ) * cos( radians( gmw_locations.longitude ) - radians( {$lng} ) ) + sin( radians( {$lat} ) ) * sin( radians( gmw_locations.latitude ) ) ),1 ) AS distance";

			$clauses['join'] .= " INNER JOIN {$wpdb->base_prefix}gmw_locations gmw_locations ON $wpdb->posts.ID = gmw_locations.object_id ";

			if ( ! empty( $distance ) ) {

				// calculate the between point.
				$bet_lat1 = $lat - ( $distance / $degree );
				$bet_lat2 = $lat + ( $distance / $degree );
				$bet_lng1 = $lng - ( $distance / ( $degree * cos( deg2rad( $lat ) ) ) );
				$bet_lng2 = $lng + ( $distance / ( $degree * cos( deg2rad( $lat ) ) ) );

				$clauses['where'] .= " AND gmw_locations.latitude BETWEEN {$bet_lat1} AND {$bet_lat2}";
				$clauses['where'] .= " AND gmw_locations.longitude BETWEEN {$bet_lng1} AND {$bet_lng2}";
				$clauses['where'] .= " AND gmw_locations.object_type = 'post'";

				// filter locations based on the distance.
				$clauses['having'] = "HAVING distance <= {$distance} OR distance IS NULL";

				// Remove extra spaces.
				$this->form['orderby'] = trim( $this->form['orderby'] );

				// If there is another order-by parameter before the distance append the distance to the orderby clause.
				if ( false !== strpos( $this->form['orderby'], ' distance' ) ) {

					$clauses['orderby'] .= ', distance ASC';

					// If there is another order-by parameter after the distance, prepend the distance first in the orderby clause.
				} elseif ( false !== strpos( $this->form['orderby'], 'distance ' ) ) {

					$clauses['orderby'] = 'distance ASC, ' . $clauses['orderby'];

					// Otherise, we order by the distance only.
				} elseif ( 'distance' === $this->form['orderby'] ) {

					$clauses['orderby'] = 'distance';
				}
			}
		} else {

			// if showing posts without location.
			if ( $this->form['query_args']['gmw_args']['showing_objects_without_location'] ) {

				// left join the location table into the query to display posts with no location as well.
				$clauses['join']  .= " LEFT JOIN {$wpdb->base_prefix}gmw_locations gmw_locations ON $wpdb->posts.ID = gmw_locations.object_id AND gmw_locations.object_type = 'post' ";
				$clauses['where'] .= " {$address_filters} ";

			} else {

				$clauses['join']  .= " INNER JOIN {$wpdb->base_prefix}gmw_locations gmw_locations ON $wpdb->posts.ID = gmw_locations.object_id ";
				$clauses['where'] .= " {$address_filters} AND ( gmw_locations.latitude != 0.000000 && gmw_locations.longitude != 0.000000 ) ";
				$clauses['where'] .= " AND gmw_locations.object_type = 'post'";
			}
		}

		// new filter.
		$clauses = apply_filters( 'gmw_ajaxfmspt_posts_query_clauses', $clauses, $this->form );

		// make sure we have groupby to only pull posts one time.
		if ( empty( $clauses['groupby'] ) ) {
			$clauses['groupby'] = $wpdb->prefix . 'posts.ID';
		}

		// add having clause.
		$clauses['groupby'] .= ' ' . $clauses['having'];

		unset( $clauses['having'] );

		return $clauses;
	}

	/**
	 * Remove total count from query.
	 *
	 * We don't need this, and can save on performance, when results disabled and showing only map.
	 *
	 * @since 1.0
	 *
	 * @param  array  $posts array of posts object.
	 * @param  string $query the sql query.
	 *
	 * @return string  modified sql query.
	 */
	public function remove_total_count( $posts, $query ) {

		$query->request = str_replace( 'SQL_CALC_FOUND_ROWS', '', $query->request );

		return $posts;
	}

	/**
	 * Main search query using WP_Query.
	 *
	 * @since 1.0
	 *
	 * @return [type] [description]
	 */
	public function search_query() {

		$tax_args  = array();
		$meta_args = array();

		// get the post types from page load settings.
		if ( $this->form['page_load_action'] ) {

			$post_types = $this->form['page_load_results']['post_types'];

			// otherwise, on form submission.
		} else {

			if ( ! empty( $this->form['form_values']['post'] ) && array_filter( $this->form['form_values']['post'] ) ) {

				$post_types = $this->form['form_values']['post'];

				// otherwise grab all the post types from the search form settings.
			} else {
				$post_types = ! empty( $this->form['search_form']['post_types'] ) ? $this->form['search_form']['post_types'] : array( 'post' );
			}
		}

		// tax query can be disable if a custom query is needed.
		if ( apply_filters( 'gmw_ajaxfmspt_enable_taxonomy_search_query', true, $this->form, $this ) ) {
			$tax_args = ! empty( $this->form['form_values']['tax'] ) ? gmw_pt_get_tax_query_args( $this->form['form_values']['tax'], $this->form ) : array();
		} else {
			$tax_args = array();
		}

		// query args.
		$this->form['query_args'] = apply_filters(
			'gmw_ajaxfmspt_search_query_args',
			array(
				'post_type'           => $post_types,
				'post_status'         => array( 'publish' ),
				'tax_query'           => $tax_args, // WPCS: slow query ok.
				'posts_per_page'      => empty( $this->form['per_page'] ) ? -1 : $this->form['per_page'],
				'paged'               => $this->form['paged'],
				'orderby'             => $this->form['orderby'],
				'order'               => 'post_modified' === $this->form['orderby'] || 'post_date' === $this->form['orderby'] ? 'DESC' : 'ASC',
				'meta_query'          => $meta_args, // WPCS: slow query ok.
				'ignore_sticky_posts' => 1,
				// we can save on performance when showing map only, without results.
				'fields'              => $this->form['results_enabled'] ? '*' : 'id=>parent',
				'gmw_args'            => $this->query_cache_args,
			),
			$this->form,
			$this
		);

		// performe tasks before the query takes place.
		$this->form = apply_filters( 'gmw_ajaxfmspt_form_before_posts_query', $this->form, $this );

		$internal_cache = GMW()->internal_cache;
		$this->query    = false;

		if ( $internal_cache ) {

			// cache key.
			$hash            = md5( wp_json_encode( $this->form['query_args'] ) );
			$query_args_hash = 'gmw' . $hash . GMW_Cache_Helper::get_transient_version( 'gmw_get_object_post_query' );
			$this->query     = get_transient( $query_args_hash );
		}

		// look for query in cache.
		if ( ! $internal_cache || empty( $this->query ) ) {

			// filter the query.
			add_filter( 'posts_clauses', array( $this, 'query_clauses' ), 20 );

			// if results disabled we can save some performance.
			if ( ! $this->form['results_enabled'] ) {
				add_filter( 'posts_pre_query', array( $this, 'remove_total_count' ), 10, 2 );
			}

			// execute the query.
			$this->query = new WP_Query( $this->form['query_args'] );

			// remove filters.
			remove_filter( 'posts_clauses', array( $this, 'query_clauses' ), 20 );

			if ( ! $this->form['results_enabled'] ) {
				remove_filter( 'posts_pre_query', array( $this, 'remove_total_count' ), 10, 2 );
			}

			// set new query in transient.
			if ( $internal_cache ) {

				/**
				 * This is a temporary solution for an issue with caching SQL requests
				 * For some reason when LIKE is being used in SQL WordPress replace the % of the LIKE
				 * with long random numbers. This SQL is still being saved in the transient. Hoever,
				 * it is not being pulled back properly when GEO my WP trying to use it.
				 * It shows an error "unserialize(): Error at offset " and the value returns blank.
				 * As a temporary work around, we remove the [request] value, which contains the long numbers, from the WP_Query and save it in the transien without it.
				 *
				 * @var [type]
				 */
				$this->query->request = null;

				set_transient( $query_args_hash, $this->query, GMW()->internal_cache_expiration );
			}
		}

		// performe some tasks.
		$this->form = apply_filters( 'gmw_ajaxfmspt_form_after_posts_query', $this->form, $this->query, $this );

		// abort if no locations found.
		if ( ! $this->query->posts ) {
			return '';
		}

		// if results disabled, skip the posts loop and pass the results as map locations.
		if ( ! $this->form['results_enabled'] ) {

			$results = '';

			foreach ( $this->query->posts as $post ) {
				if ( isset( $post->location_id ) ) {
					$this->map_locations[] = $this->get_map_location( $post, $post );
				}
			}

			// otherwise, run posts loop.
		} else {

			$this->form['results']       = $this->query->posts;
			$this->form['results_count'] = $this->query->post_count;
			$this->form['total_results'] = $this->query->found_posts;
			$this->form['max_pages']     = $this->query->max_num_pages;

			$gmw = $this->form;

			ob_start();

			global $post;

			// enable post filter.
			add_filter( 'the_post', array( $this, 'the_post' ), 15, 2 );

			while ( $this->query->have_posts() ) :

				$this->query->the_post();

				// if post has location, add it to the map locations array.
				if ( $this->form['map_enabled'] && isset( $post->location_id ) ) {
					$this->map_locations[] = $this->get_map_location( $post, $post );
				}

				// add class attribute to object.
				$post->class_attr = $this->get_class_attr( $post );

				include $this->form['results_template']['content_path'] . 'single-result.php';

			endwhile;

			remove_filter( 'the_post', array( $this, 'the_post' ), 15, 2 );

			wp_reset_postdata();

			$results = ob_get_clean();
		}

		// return HTML results.
		return $results;
	}

	/**
	 * Modify the post object if needed.
	 *
	 * @param  object $post the post object in the loop.
	 *
	 * @param  object $query   the WP_Query object.
	 *
	 * @return the modified post object.
	 */
	public function the_post( $post, $query ) {
		return apply_filters( 'gmw_ajaxfmspt_posts_loop_single_post', $post, $this->form, $query );
	}
}
