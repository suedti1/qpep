<?php
/**
 * GMW AJAX Forms - Posts Locator form settings.
 *
 * @package geo-my-wp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * GMW_Posts_Locator_AJAX_Forms_Form_Settings class
 */
class GMW_Posts_Locator_Ajax_Forms_Form_Settings {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		add_filter( 'gmw_ajaxfmspt_form_settings_orderby_options', array( $this, 'orderby_options' ), 20, 3 );

		// defaults settings.
		add_filter( 'gmw_posts_locator_ajax_form_default_settings', array( $this, 'set_defaults' ), 10 );

		// form fields.
		add_filter( 'gmw_posts_locator_ajax_form_settings', array( $this, 'form_fields' ), 10 );
		add_action( 'gmw_posts_locator_ajax_form_settings_ajaxfms_search_form_taxonomies', array( 'GMW_Form_Settings_Helper', 'taxonomies' ), 5, 3 );
		add_action( 'gmw_posts_locator_ajax_form_settings_ajaxfms_post_excerpt', array( 'GMW_Form_Settings_Helper', 'excerpt' ), 10, 2 );

		// validations.
		add_filter( 'gmw_posts_locator_ajax_validate_form_settings_ajaxfms_post_excerpt', array( 'GMW_Form_Settings_Helper', 'validate_excerpt' ), 10 );
	}

	/**
	 * Orderby settings values.
	 *
	 * @param  array $options options.
	 *
	 * @param  array $form_fields form fields.
	 *
	 * @param  array $form gmw form.
	 *
	 * @return [type]          [description]
	 */
	public function orderby_options( $options, $form_fields, $form ) {

		return array(
			'options'           => array(
				'distance'      => __( 'Distance', 'gmw-ajax-forms' ),
				'ID'            => __( 'Post id', 'gmw-ajax-forms' ),
				'post_title'    => __( 'Post title', 'gmw-ajax-forms' ),
				'post_date'     => __( 'Date created', 'gmw-ajax-forms' ),
				'post_modified' => __( 'Last modified', 'gmw-ajax-forms' ),
				'post_type'     => __( 'Post type', 'gmw-ajax-forms' ),
			),
			'default_option'    => 'distance:Distance,post_title:Name,post_modified:Last Updated',
			'available_options' => 'distance, post_title, ID ( post ID ), post_date ( date created ), post_modified ( last modifed ) and post_type',
		);
	}

	/**
	 * Default settings
	 *
	 * @param  [type] $settings [description].
	 *
	 * @return [type]           [description]
	 */
	public function set_defaults( $settings ) {

		$settings['page_load_results']['post_types'] = array( 'post' );
		$settings['page_load_results']['orderby']    = 'distance';
		$settings['search_form']['post_types']       = array( 'post' );
		$settings['search_form']['taxonomies']       = array();
		$settings['search_results']['orderby']       = 'distance:Distance,post_title:Name,post_modified:Last Updated';

		return $settings;
	}

	/**
	 * Form fields
	 *
	 * @param  [type] $form_fields [description].
	 *
	 * @return [type]              [description]
	 */
	public function form_fields( $form_fields ) {

		$form_fields['page_load_results']['post_types'] = array(
			'name'     => 'post_types',
			'type'     => 'multiselect',
			'options'  => GMW_Form_Settings_Helper::get_post_types(),
			'default'  => array( 'post' ),
			'label'    => __( 'Post Types', 'gmw-ajax-forms' ),
			'desc'     => __( 'Select the post types to display on page load.', 'gmw-ajax-forms' ),
			'priority' => 12,
		);

		$form_fields['search_form']['post_types'] = array(
			'name'        => 'post_types',
			'type'        => 'multiselect',
			'default'     => array( 'post' ),
			'label'       => __( 'Post Types', 'geo-ajax-forms' ),
			'placeholder' => __( 'Select post types', 'gmw-ajax-forms' ),
			'desc'        => __( 'Select a single post type to set as the default, or multiple post types to display as a dropdown select box in the search form.', 'gmw-ajax-forms' ),
			'options'     => GMW_Form_Settings_Helper::get_post_types(),
			'attributes'  => '',
			'priority'    => 12,
		);

		$form_fields['search_form']['taxonomies'] = array(
			'name'       => 'taxonomies',
			'type'       => 'function',
			'default'    => '',
			'function'   => 'ajaxfms_search_form_taxonomies',
			'label'      => __( 'Taxonomies', 'gmw-ajax-forms' ),
			'desc'       => __( 'Setup the taxonomies in the search form.', 'gmw-ajax-forms' ),
			'attributes' => array(),
			'priority'   => 14,
		);

		$form_fields['search_results']['location_meta'] = array(
			'name'        => 'location_meta',
			'type'        => 'multiselect',
			'default'     => '',
			'label'       => __( 'Location Meta', 'gmw-ajax-forms' ),
			'placeholder' => __( 'Select location metas', 'gmw-ajax-forms' ),
			'desc'        => __( 'Select the the location meta fields which you would like to display for each location in the list of results.', 'gmw-ajax-forms' ),
			'options'     => array(
				'phone'   => __( 'Phone', 'gmw-ajax-forms' ),
				'fax'     => __( 'Fax', 'gmw-ajax-forms' ),
				'email'   => __( 'Email', 'gmw-ajax-forms' ),
				'website' => __( 'Website', 'gmw-ajax-forms' ),
			),
			'attributes'  => '',
			'priority'    => 36,
		);

		$form_fields['search_results']['opening_hours'] = array(
			'name'       => 'opening_hours',
			'type'       => 'checkbox',
			'default'    => '',
			'label'      => __( 'Hours of Operation', 'gmw-ajax-forms' ),
			'cb_label'   => __( 'Enable', 'gmw-ajax-forms' ),
			'desc'       => __( 'Display opening days & hours for each location in the list of results.', 'gmw-ajax-forms' ),
			'attributes' => '',
			'priority'   => 38,
		);

		$form_fields['search_results']['excerpt'] = array(
			'name'       => 'excerpt',
			'type'       => 'function',
			'default'    => '',
			'label'      => __( 'Excerpt', 'gmw-ajax-forms' ),
			'cb_label'   => '',
			'desc'       => __( 'Display excerpt in each location in the results.', 'gmw-ajax-forms' ),
			'attributes' => '',
			'priority'   => 40,
		);

		$form_fields['search_results']['taxonomies'] = array(
			'name'       => 'taxonomies',
			'type'       => 'checkbox',
			'default'    => '',
			'label'      => __( 'Taxonomies', 'gmw-ajax-forms' ),
			'cb_label'   => __( 'Enable', 'gmw-ajax-forms' ),
			'desc'       => __( 'Check this checkbox to display the taxonomies and terms associate with each post in the list of results.', 'gmw-ajax-forms' ),
			'attributes' => '',
			'priority'   => 65,
		);

		$form_fields['info_window']['location_meta'] = array(
			'name'       => 'location_meta',
			'type'       => 'multiselect',
			'default'    => '',
			'label'      => __( 'Location Meta', 'gmw-ajax-forms' ),
			'desc'       => __( 'Select the location meta fields to display in the info-window.', 'gmw-ajax-forms' ),
			'options'    => GMW_Form_Settings_Helper::get_location_meta(),
			'attributes' => array(),
			'priority'   => 90,
		);

		$form_fields['info_window']['excerpt'] = array(
			'name'       => 'excerpt',
			'type'       => 'function',
			'function'   => 'ajaxfms_post_excerpt',
			'label'      => __( 'Excerpt', 'gmw-ajax-forms' ),
			'default'    => '',
			'desc'       => __( 'Show the post\'s excerpt.', 'gmw-ajax-forms' ),
			'attributes' => array(),
			'priority'   => 95,
		);

		return $form_fields;
	}
}
new GMW_Posts_Locator_Ajax_Forms_Form_Settings();
