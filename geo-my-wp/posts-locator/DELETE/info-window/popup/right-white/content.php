<?php
/**
 * Popup "left-white" info-window template file .
 *
 * The content of this file will be displayed in the map markers' info-window.
 *
 * This file can be overridden by copying it to
 *
 * your-theme's-or-child-theme's-folder/geo-my-wp/posts-locator/ajax-forms/info-window/popup/
 *
 * @see
 * @param $gmw  - the form being used ( array )
 * @param $post - the post being displayed ( object )
 */
?>
<?php do_action( 'gmw_info_window_before', $post, $gmw ); ?>  

<div id="gmw-toggle-button-wrapper">
	<?php gmw_right_element_toggle_button( '#gmw-popup-info-window', '320px' ); ?>
</div>

<div class="gmw-info-window-header">

	<?php gmw_element_close_button( 'gmw-icon-cancel' ); ?>

	<?php do_action( 'gmw_info_window_before_title', $post, $gmw ); ?>

	<a class="title" href="<?php the_permalink( $post->ID ); ?>" >
		<?php echo esc_html( $post->post_title ); ?>
	</a>

	<?php gmw_info_window_distance( $post, $gmw ); ?>

</div>

<div class="gmw-info-window-inner popup template-content-wrapper">

	<?php do_action( 'gmw_info_window_start', $post, $gmw ); ?>

	<?php gmw_info_window_featured_image( $post, $gmw ); ?>	

	<?php do_action( 'gmw_info_window_before_address', $post, $gmw ); ?>

	<?php gmw_info_window_address( $post, $gmw ); ?>

	<?php gmw_info_window_directions_link( $post, $gmw ); ?>

	<?php do_action( 'gmw_info_window_before_excerpt', $post, $gmw ); ?>

	<?php gmw_info_window_post_excerpt( $post, $gmw ); ?>

	<?php do_action( 'gmw_info_window_before_location_meta', $post, $gmw ); ?>

	<?php gmw_info_window_location_meta( $post, $gmw, false ); ?>

	<?php gmw_info_window_directions_system( $post, $gmw ); ?>

	<?php do_action( 'gmw_info_window_end', $post, $gmw ); ?>	

</div>  

<?php do_action( 'gmw_info_window_after', $post, $gmw ); ?>
