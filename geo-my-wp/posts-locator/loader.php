<?php
/**
 * GEO my WP AJAX Forms extension - Posts Locator Loader file.
 *
 * @package gmw-ajax-forms
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include form settings for posts locator ajax forms only.
 *
 * @param  [type] $form [description]
 * @return [type]       [description]
 */
if ( IS_ADMIN ) {
	include 'includes/admin/class-gmw-posts-locator-ajax-forms-form-settings.php';
}

/**
 * Include Posts Locator search query and functions
 *
 * @since 1.0
 *
 * @param  [type] $form gmw form.
 */
function gmw_ajaxfmspt_search_functions( $form ) {
	include_once 'includes/class-gmw-posts-locator-ajax-form.php';
	include_once GMW_PT_PATH . '/includes/gmw-posts-locator-search-form-template-functions.php';
	include_once GMW_PT_PATH . '/includes/gmw-posts-locator-search-results-template-functions.php';
}
add_action( 'gmw_ajaxfmspt_form_init', 'gmw_ajaxfmspt_search_functions' );

/**
 * Include info-window functions.
 *
 * @since 1.0
 *
 * @param  object $location location object.
 * @param  array  $gmw      gmw form.
 */
function gmw_ajaxfmspt_get_info_window_data( $location, $gmw ) {
	include_once 'includes/gmw-posts-locator-ajax-info-window-functions.php';
}
add_action( 'gmw_ajaxfmspt_ajax_info_window_init', 'gmw_ajaxfmspt_get_info_window_data', 20, 2 );
