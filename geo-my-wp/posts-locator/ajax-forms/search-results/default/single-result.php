<?php 
/**
 * Posts locator Ajax "default" single result template file.
 *
 * This file outputs each result of the list of results.
 *
 * This file can be overridden by copying the entire "default" folder with all its files into
 *
 * your-theme's-or-child-theme's-folder/geo-my-wp/posts-locator/ajax-forms/search-results/
 *
 * @param $gmw  ( array ) the form being used
 * @param $post ( object ) post object in the loop
 */
?>	
<li id="gmw-single-post-<?php echo esc_attr( $post->ID ); ?>" class="<?php gmw_ajaxfms_search_results_class_attr( $post, $gmw ); ?>">

	<?php do_action( 'gmw_results_single_item_start', $post, $gmw ); ?>

		  <div class="single-loop-item">	

										<div class="taxonomies">	
										
										<?php 

    $fueltype = wp_get_post_terms(get_the_id(  ), 'category');

foreach( $fueltype as $term ) {
    // Get the term link
    $term_link = get_term_link( $term );

   
    echo '<span class="bttn cat">'. $term->name .'</span>';
    		
} 


    $loc = wp_get_post_terms(get_the_id(  ), 'city');
$cc=0;
foreach( $loc as $term ) { 
    // Get the term link
    $term_link = get_term_link( $term );

   
//    echo '<a href="'. get_term_link( $term ) .'" class="bttn location">'. $term->name .'</a>';
if ($cc<1){
    echo '<span class="bttn location">'. $term->name .'</span>';
}
    $cc=$cc+1;		
} 


if(strpos( $_SERVER['HTTP_HOST'], 'quiprimaepoi.it') !== false){
$urlvar='&nologo=1';
}
else{
	$urlvar='?nologo=1';

}

?>
										<div class="details-popup"><a   class="inline" href="<?php the_permalink();echo $urlvar; ?>">+</a></div>

											
											
										</div>
											<div class="medium-8 columns loop-item">

											<h1><a class="inline" href="<?php the_permalink();echo $urlvar; ?>"><?php the_title(); ?></a></h1> 
											<p><?php the_excerpt(); ?></p>
											<a class="mmore inline"  href="<?php the_permalink();echo $urlvar; ?>"><?php _e( 'Leggi di pi&ugrave;', 'Parma' ); ?></a>
											</div>		
											
										<div class="medium-1 columns">
											&nbsp;
										</div>
												
										<div class="medium-2 columns">
											
											
				

 <?php 
$images = get_field('file');

    if( $images ): 
?>     
    						
						<div class="img-list-img">					
            <a class="inline" href="<?php the_permalink();echo $urlvar; ?>" class="img-list-img">
   <?php 
        $image_1 = $images[0]; 
        ?>       
	            
                <img src="<?php echo $images[0]['sizes']['home-list']; ?>"  alt="<?php echo $image['alt']; ?>" />
                
            </a>								
											</div>	
											
<?php endif; ?>
				
			
											
											
											
													
										</div>
										<div class="medium-1 columns">
											
										</div>
																			<div class="break">	</div>

									  </div>



	<?php do_action( 'gmw_results_single_item_end', $post, $gmw ); ?>
</li>
