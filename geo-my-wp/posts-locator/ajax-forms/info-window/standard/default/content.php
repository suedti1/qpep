<?php
/**
 * Standard "default" info-window template file.
 *
 * The content of this file will be displayed in the map markers' info-window.
 *
 * This file can be overridden by copying it to
 *
 * your-theme's-or-child-theme's-folder/geo-my-wp/posts-locator/ajax-forms/info-window/standard/
 *
 * @see
 * @param $gmw  - the form being used ( array )
 * @param $post - the post being displayed ( object )
 */
?>
<div class="gmw-info-window-inner standard">

	<?php do_action( 'gmw_info_window_start', $post, $gmw ); ?>

	<?php gmw_info_window_featured_image( $post, $gmw ); ?>	

	<?php do_action( 'gmw_standard_iw_template_before_title', $post, $gmw ); ?>

	<a class="title" href="<?php echo get_permalink( $post->ID ); ?>" >
		<?php echo esc_html( $post->post_title ); ?>
	</a>

	<?php do_action( 'gmw_info_window_before_address', $post, $gmw ); ?>

	<?php gmw_info_window_address( $post, $gmw ); ?>

	<?php gmw_info_window_directions_link( $post, $gmw ); ?>

	<?php gmw_info_window_distance( $post, $gmw ); ?>

	<?php do_action( 'gmw_info_window_before_excerpt', $post, $gmw ); ?>

	<?php gmw_info_window_post_excerpt( $post, $gmw ); ?>

	<?php do_action( 'gmw_info_window_before_location_meta', $post, $gmw ); ?>

	<?php gmw_info_window_location_meta( $post, $gmw, false ); ?>

	<?php do_action( 'gmw_info_window_end', $post, $gmw ); ?>

</div>  
