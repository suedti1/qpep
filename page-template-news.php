<?php
/**
* Template Name: News page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header(); ?>

                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="row type-txt-small-sans text-below">
                            <div class="columns large-2">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <div class="columns large-5">
	<div id="news-page-title">                            
<?php 
	
	the_content( );
	
	echo '</div>';
	// WP_Query arguments
$args = array(
	'post_type'              => array( 'news' ),
);

// The Query
$digi_artcl = new WP_Query( $args );

// The Loop
if ( $digi_artcl->have_posts() ) {
	while ( $digi_artcl->have_posts() ) {
		$digi_artcl->the_post();
		
		
				echo'	
									<div class="medium-12 columns home-news-item">

											
											<h3>'.get_field("news_date").'</h3> 
											<h1>
							'.get_the_title().'  </h1><br><br>
											<p>'.get_the_content().'</p> 
											                  <div class="news-img">'. get_the_post_thumbnail( $page->ID, 'thumbnail' ).'
<br><br><br><br>
										</div>
									';
										}
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();
?>
                            </div>
                                                        <div class="columns large-2">
</div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>

<?php get_footer(); ?>