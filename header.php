        <!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>"/> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/> 
                           <link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon-32x32.png" sizes="32x32" />
							<link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon-16x16.png" sizes="16x16" />

        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body class="<?php echo implode(' ', get_body_class()); ?>">
        <div class="pagewrapper">
            <!-- Header and Nav -->
            <div class="nav-placeholder"></div>
            <header id="Header" class="hides"> 
                <div class="row drug	">
                                    
						<?php if ( ($_GET["nologo"]!=1) ) : ?>

								    		 <?php    if( ICL_LANGUAGE_CODE != 'it'  ) { $llogo="logo-parther.gif";$sstyle="height:55px; top:37px";
 }
								    		 else{
									    		  $llogo="logo1.svg";
									    		 
								    		 }
								    		 
								    		 ?>

	                          <div class="columns large-2">
		                         <?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );?>
<a href="<?php echo $my_home_url; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ).'/img/'.$llogo; ?>" style="<?php echo $sstyle; ?>" class="head-logo float-right">                            </a>
							  </div> 


                            <?php endif; ?>
                                 <div class="large-10 columns small-6">

						<?php if ( ($_GET["nologo"]!=1) ) : ?>

<?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>

            <?php do_action('icl_language_selector'); ?>

                        <div id="fs-navbar-1-1" class="fs-navbar-1-1">
                            <input id="fs-checkbox-trigger" type="checkbox" class="fs-checkbox-trigger">
                            <label for="fs-checkbox-trigger" class="menu-btn-mob">
                                <span class="fs-navbar-icon right round  effect-slide-in-top">                     
								</span>
                            </label>
                            
                            <header class="fs-navbar">
                                <nav class="mob-nav">
                                    <ul>
	                                                                            <li>
                                           <!-- <a href="/?page_id=2/" class="btn-proposta2 tr2"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/btn_partecipa_top.svg" style="float: right;width: 300px;"></a>-->
                                        </li>
                                        <li>
                                            <a href="/" class="nav-button" title="/"><?php _e( 'Homepage', 'Parma' ); ?> </a>
                                        </li>
                                        <li>
                                            <a href="/" class="nav-button" title="partecipa"><?php _e( 'Partecipa', 'Parma' ); ?></a>
                                        </li>
                                        <li>
                                            <a href="/" class="nav-button" title="esplora"><?php _e( 'Il progetto ', 'Parma' ); ?></a>
                                        </li>
                                        <li>
                                            <a href="http://www.parther.eu/" class="nav-button" title="Parther.eu"><?php _e( 'Parther.eu', 'Parma' ); ?></a>
                                        </li>

                                    </ul>
                                </nav>
                            </header>
                        </div>
                    <button style="visibility:hidden" class="btn-menu"/>Menu</button>
                    <?php endif; ?>



                    </div>


                </div>
            </header>
                               
            <div class="row wp-site-content">