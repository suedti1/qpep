<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>
										
		
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>

		
			  <div class="single-loop-item">	

										<div class="taxonomies">	
										
										<?php 

    $fueltype = wp_get_post_terms(get_the_id(  ), 'category');

foreach( $fueltype as $term ) {
    // Get the term link
    $term_link = get_term_link( $term );

   
    echo '<a href="'. get_term_link( $term ) .'" class="bttn cat">'. $term->name .'</a>';
    		
} 

?>

											
											
											 <a href ="#" class="bttn location"><?php the_field("luogo"); ?></a>
										</div>
											<div class="medium-8 columns loop-item">

											<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1> 
											<p><?php the_excerpt(); ?></p>
											<div class="mmore">Leggi di più...</div>
											</div>			
										<div class="medium-3 columns">
											
											
										
						<div class="img-list-img">					
            <a href="<?php the_permalink(); ?>" class="img-list-img">

 <?php 
$images = get_field('file');

    if( $images ): 
        $image_1 = $images[0]; 
?>                
	            
                <img src="<?php echo $images[0]['sizes']['home-list']; ?>"  alt="<?php echo $image['alt']; ?>" />
           
<?php endif; ?>
				
			 </a>								
											</div>	
											
											
											
													
										</div>
										<div class="medium-1 columns">
											
											<div class="details-popup"><a data-fancybox data-width="100%" data-height="680" id="p6" href="<?php the_permalink(); ?>">+</a></div>
										</div>
																			<div class="break">	</div>

									  </div>

		
		
		
		<?php
	}
	?>
	

	
	<!--
	Found <?php echo $query->found_posts; ?> Results<br />
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>

	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	-->
	<?php
}
else
{
	echo "No Results Found";
}
?>